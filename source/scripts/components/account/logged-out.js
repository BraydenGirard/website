import TemplateElement from '../TemplateElement.js'

class LoggedOut extends TemplateElement {
  constructor () {
    super()
    this.template = '/scripts/components/account/logged-out.html'
  }
}

window.customElements.define('app-account-logged-out', LoggedOut)
export default LoggedOut
