const template = document.createElement('template')
template.innerHTML = '<slot></slot>'

class Link extends HTMLElement {
  constructor () {
    super()

    this.addEventListener('click', (event) => {
      const anchor = event.target.closest('a')
      if (!anchor) return
      event.preventDefault()
      const href = anchor.getAttribute('href')
      const destination = new URL(href, location)
      if (window.origin === destination.origin) {
        window.history.pushState(null, '', destination)
        window.dispatchEvent(new PopStateEvent('popstate'))
      } else {
        location.assign(destination)
      }
    })
  }

  connectedCallback () {
    const root = this.attachShadow({mode: 'open'})
    const clone = document.importNode(template.content, true)
    root.appendChild(clone)
  }
}

window.customElements.define('app-link', Link)
export default Link
