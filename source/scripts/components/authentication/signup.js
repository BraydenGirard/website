import TemplateElement from '../TemplateElement.js'
import { Client } from '../../auth0-api.js'

class Signup extends TemplateElement {
  constructor () {
    super()
    this.template = '/scripts/components/authentication/signup.html'
  }

  connectedCallback () {
    super.connectedCallback()
    this.shadowRoot.addEventListener('submit', this._onSubmit)
  }

  disconnectedCallback () {
    this.shadowRoot.removeEventListener('submit', this._onSubmit)
  }

  async _onSubmit (event) {
    event.preventDefault()

    this.querySelector('#progress').removeAttribute('hidden')
    this.querySelector('#problem').setAttribute('hidden', true)

    const username = event.target.querySelector('#username').value
    const email = event.target.querySelector('#email').value
    const password = event.target.querySelector('#password').value

    const auth0 = new Client()

    let accessToken
    try {
      await auth0.signup(email, password, username)
      const response = await auth0.getTokenByPassword(email, password)
      accessToken = response.access_token
    } catch (error) {
      this.querySelector('#problem output').textContent = error
      this.querySelector('#progress').setAttribute('hidden', true)
      this.querySelector('#problem').removeAttribute('hidden')
      return
    }

    localStorage.setItem('app.accessToken', accessToken)
    localStorage.setItem('app.username', username)
    localStorage.setItem('app.email', email)

    window.history.pushState(null, '', '/sites')
    window.dispatchEvent(new PopStateEvent('popstate'))
  }
}

window.customElements.define('app-signup', Signup)
export default Signup
