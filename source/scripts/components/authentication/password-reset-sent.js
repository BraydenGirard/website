import TemplateElement from '../TemplateElement.js'

class PasswordResetSent extends TemplateElement {
  constructor () {
    super()
    this.template = '/scripts/components/authentication/password-reset-sent.html'
  }
}

window.customElements.define('app-password-reset-sent', PasswordResetSent)
export default PasswordResetSent
