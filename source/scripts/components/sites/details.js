import TemplateElement from '../TemplateElement.js'

class SitesDetails extends TemplateElement {
  constructor () {
    super()
    this.template = '/scripts/components/sites/details.html'
  }
}

window.customElements.define('app-sites-details', SitesDetails)
export default SitesDetails
